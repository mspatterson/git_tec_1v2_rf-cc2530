/***********************************************************************************
  Filename: light_switch.c

  Description:  
	This application function as a radio receiver/transmitter.
	It can also program the MSP430.  
  

***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/
#include <hal_lcd.h>
#include <hal_led.h>
#include <hal_joystick.h>
#include <hal_assert.h>
#include <hal_board.h>
#include <hal_int.h>
#include "hal_mcu.h"
#include "hal_button.h"
#include "hal_rf.h"
#include "util_lcd.h"
#include "basic_rf.h"
#include "hal_timer_32k.h"
#include "hal_uart.h"
#include "CRC.h"
#include <stdbool.h>
/***********************************************************************************
* CONSTANTS
*/
//Device Role - only one must be defined
#define PROGRAM_END_DEVICE	1

// Application parameters
#define RF_CHANNEL                11	//25      // 2.4 GHz RF channel

// BasicRF address definitions
#define PAN_ID                0x2007
#define SWITCH_ADDR           0x2520
#define LIGHT_ADDR            0xBEEF
#define APP_PAYLOAD_LENGTH        156
#define LIGHT_TOGGLE_CMD          0XDD

// Application states
#define IDLE                      0
#define SEND_CMD                  1

#define ANTENNA_A					0
#define ANTENNA_B					1

#define IDLE                      0
#define POLL_UART_DATA	          0xAA

#define RF_TX_PERIOD			 16     //16->0.512ms	// 32->0.97ms	// 313 -> 10ms
#define STATS_LENGTH			100
#define RF_HEADER_BYTES			10

#define BSL_MODE_TIMEOUT		9000*2
#define FIRST_COMM_TIMEOUT		10000   // 10000 = 5sec   //35000
#define SECOND_COMM_TIMEOUT             63000   // 60000 = 30 sec

/**********************************************************
Baud rate (bps) UxBAUD.BAUD_M UxGCR.BAUD_E Error(%)
2400 				59 			6 			0.14
9600 				59 			8 			0.14
57600 				216 		10 			0.03
115200 				216 		11 			0.03
230400 				216 		12 			0.03

Commonly used Baud Rate Settings for 32 MHz System Clock*/

// BAUD RATE 115,200
#define uartBaudM		216
#define uartBaudE		11

// BAUD RATE 9600
#define uartBaudM_9600		59
#define uartBaudE_9600		8

#define RX_DATA_SIZE		50

// PACKET HEADER DEFINES
#define HOST_PACKET_HEADER	0X46
#define WTTTS_PACKET_HEADER 	0X29
#define ENTER_BSL_MODE		0X90
#define BSL_PROGRAM_DATA	0X92
#define BSL_MODE_ENTERED	0X94
#define APP_MODE_ENTERED	0X96
#define ENTER_APP_MODE		0X98

#define	BSL_MODE_ENTERED_LENGTH	14
#define	APP_MODE_ENTERED_LENGTH	13

#define TX_ACK			0XB3
#define TX_NACK			0XDC
#define ENTER_PM_3		0XA7
#define POWER_MODE3		0x03

#define COMMAND			0X53
#define TX_DATA			0X29
#define RADIO_CHANGE_CHANNEL	0XA9
#define RADIO_CHANGE_POWER	0XA6
#define RADIO_RCVR_ON           0XA1
#define RADIO_RCVR_OFF          0XA3
#define RADIO_RCVR_ON_TIMED     0XA2
#define RADIO_RCVR_OFF_TIMED    0XA4
#define RADIO_SET_ADDRESS       0XA5
#define RADIO_SET_PAN_ID        0XA8
#define RADIO_SET_TIMEOUT       0XCA

#define RESET_TIMES_BEFORE_SLEEP    2
#define UART_POLLING_INTERVAL     3
/***********************************************************************************
* LOCAL VARIABLES
*/

// Define and allocate a setup structure for the UART protocol:
typedef struct {
	unsigned char uartNum : 1; // UART peripheral number (0 or 1)
	unsigned char START : 1; // Start bit level (low/high)
	unsigned char STOP : 1; // Stop bit level (low/high)
	unsigned char SPB : 1; // Stop bits (0 => 1, 1 => 2)
	unsigned char PARITY : 1; // Parity control (enable/disable)
	unsigned char BIT9 : 1; // 9 bit enable (8bit / 9bit)
	unsigned char D9 : 1; // 9th bit level or Parity type
	unsigned char FLOW : 1; // HW Flow Control (enable/disable)
	unsigned char ORDER : 1; // Data bit order(LSB/MSB first)
} UART_PROT_CONFIG;
UART_PROT_CONFIG __xdata uartProtConfig;

#define SIZE_OF_UART_RX_BUFFER 50
#define SIZE_OF_UART_TX_BUFFER SIZE_OF_UART_RX_BUFFER
// Allocate buffer+index for UART RX/TX
unsigned short __xdata uartRxBuffer[SIZE_OF_UART_RX_BUFFER];
unsigned short __xdata uartTxBuffer[SIZE_OF_UART_TX_BUFFER];
unsigned short __xdata uartRxIndex, uartTxIndex;

static uint8 pTxData[APP_PAYLOAD_LENGTH];
static uint8 pRxData[APP_PAYLOAD_LENGTH];
static basicRfCfg_t basicRfConfig;

static uint8  bAntennaState;
static volatile uint8 appState;
const char cAtmelString[]="DATA 0055,0,30:";
static unsigned char TxBuffer[RX_DATA_SIZE];
static unsigned char RxBuffer[RX_DATA_SIZE];
static uint8	RxBufIndex; 
static uint32 mSecCounter1,mSecCounter2,mSecCounter3 ;
static uint8  bBSLMode;
static uint8 bPacketLength =0;
static uint8 bBslTxRetry = 0;
uint16 wPacketLen;
static bool bReceiverOff = false;
static uint8 bResetCounter;
static uint8 mSecPollUart;


#ifdef SECURITY_CCM
// Security key
//static uint8 key[]= {
//    0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
//    0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
//};
static uint8 key[]= {
    0xdf, 0xd0, 0xde, 0xd1, 0xdd, 0xd2, 0xdc, 0xd3,
    0x3d, 0xcd, 0x2d, 0x43, 0x87, 0x0f, 0x19, 0x82,
};
#endif

/***********************************************************************************
* LOCAL FUNCTIONS
*/
static void appSwitch();


void appSelectAntenna(uint8 bAntennaSel);
void appToggleAntennaSwitch(void);
static void appTimerISR(void);
static void appConfigTimer(uint16 cycles);

static void appUART0Init(void);
void uartInitProtocol(UART_PROT_CONFIG* uartProtConfig) ;
void uart0Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength);

void Reset_MSP430F5419A_BSL(void);
void Reset_MSP430F5419A_Application(void);
void GetMSP430_BSL_Version(void);
static void appUART1Init(void);
void uart1Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength);
uint16 GetMSP430_BSL_BufferSize(void);
void rRxBufGetAllData( char *buf);
void RxBufPut( char data);
void Uart1EnableRxInterrupt(void);
void RxBufReset(void);
void SetMSP430_BSL_Password(void);
void MSP430_BSL_Erase(void);
void MSP430_BSL_Get_Buf_Size(void);

uint8 WriteBSLModeEnteredResp(uint8 *pData);
uint8 WriteAppModeEnteredResp(uint8 *pData);
bool SendRfPacket(uint8 *pData, uint16 wPackLen);
void ParseRfPAcket(uint8 *pData);
uint8 WriteProgramDataResp(uint8 *pData);
void SendDataToMcu(unsigned char* TxBuf, unsigned short TxBufLength);
void PowerDownMode(void);

void TestBSLMode(void);

/***********************************************************************************
* @fn          appSwitch
*
* @brief       Application code for switch application. Puts MCU in
*              endless loop to wait for commands from from switch
*
* @param       basicRfConfig - file scope variable. Basic RF configuration data
*              pTxData - file scope variable. Pointer to buffer for TX
*              payload
*              appState - file scope variable. Holds application state
*
* @return      none
*/
static void appSwitch()
{
	uint16 wUartRcvdBytesOld = 0;
	uint16  wUartRcvdBytesNew =0;
	uint16 wAppPayload;
        uint16 wMcuResetTimeout;        // if the radio uart is idle for more than "timeout" ms it will reset the MCU (MSP430)
    
	pTxData[0] = LIGHT_TOGGLE_CMD;
        
        bResetCounter = 0;
        wMcuResetTimeout = FIRST_COMM_TIMEOUT;
        
//        P0DIR&=~BIT3;
	
//	halUartInit(HAL_UART_BAUDRATE_115200, 1);
//	halUartInit(HAL_UART_BAUDRATE_38400, 1);
//	halUartInit(HAL_UART_BAUDRATE_115966, 1);
//        halUartInit(HAL_UART_BAUDRATE_250000, 1);
        halUartInit(HAL_UART_BAUDRATE_125000, 1);
	
	// set the BSL TX RX as 3state inputs for now, se they don't interfere with ADC_DRDY
//	P0INP|=BIT4|BIT5;
	appUART1Init();
	P0INP&=~(BIT4|BIT5);
	uartInitProtocol(&uartProtConfig) ;
	
    // Initialize BasicRF
    basicRfConfig.myAddr = SWITCH_ADDR;
    if(basicRfInit(&basicRfConfig)==FAILED) {
      HAL_ASSERT(FALSE);
    }
	
	basicRfReceiveOn();
    // Keep Receiver off when not needed to save power
//    basicRfReceiveOff();
	halTimer32kIntEnable();

	// poll the UART RX buffer every 1ms, and if we have not received anything for 
	// the last 1 ms period transmit the data over the air
    // Main loop
//	halLedSet(1);
        halLedClear(1);

	// set a timer for main MCU communication timeout
	// if the Radio does not receive data for COMM_TIMEOUT - resets the main MCU
	mSecCounter2 = wMcuResetTimeout;
	
    while (TRUE) 
	{
      
      if(!bBSLMode)
      {
        if( appState == POLL_UART_DATA )
        {
          // if there is new data on UART
          wUartRcvdBytesNew = halUartGetNumRxBytes();
          
          if (wUartRcvdBytesOld)
          {
            if(wUartRcvdBytesOld == wUartRcvdBytesNew)	
            {
              P1_3 = 1;       //signal that is not ready to receive on UART
              
              if(wMcuResetTimeout == FIRST_COMM_TIMEOUT)
                wMcuResetTimeout = SECOND_COMM_TIMEOUT;
              
              mSecCounter2 = wMcuResetTimeout;
              wAppPayload = halUartRead(pTxData, wUartRcvdBytesNew);
              wUartRcvdBytesOld = 0;
              switch(pTxData[0])
              {
              case TX_DATA:	
                //halUartEnableRxFlow(0); //signal that is not ready to receive on UART
                //halLedSet(1);
                // if the transmission did not get ACK switch the antenna
                if(basicRfSendPacket(LIGHT_ADDR, &pTxData[1], wAppPayload-1)==FAILED)
                {
                  appToggleAntennaSwitch();
                  if(basicRfSendPacket(LIGHT_ADDR, &pTxData[1], wAppPayload-1)==FAILED)
                  {
                    appToggleAntennaSwitch();
                    if(basicRfSendPacket(LIGHT_ADDR, &pTxData[1], wAppPayload-1)==FAILED)
                    {
                      appToggleAntennaSwitch();
                    }
                  }
                }
                //halUartEnableRxFlow(1); //signal that is ready to receive on UART
                break;
              case COMMAND:
                switch(pTxData[1])
                {
                case ENTER_PM_3:
                  PowerDownMode();
                  break;
                case RADIO_CHANGE_CHANNEL:
                  halRfSetChannel(pTxData[2]);
                  break;
                case RADIO_CHANGE_POWER:
                  TXPOWER = pTxData[2];									
                  break;
                case RADIO_RCVR_OFF:
                  basicRfReceiveOff();
                  break;                                                                                
                case RADIO_RCVR_ON:
                  //halLedSet(1);
                  basicRfReceiveOn();
                  break;       
                case RADIO_RCVR_OFF_TIMED:
                  basicRfReceiveOff();
                  mSecCounter3 = pTxData[3];
                  mSecCounter3 = mSecCounter3<<8; 
                  mSecCounter3 |= pTxData[2];
                  bReceiverOff = true;  
                  break;                                                                                
                case RADIO_SET_ADDRESS:
                  basicRfConfig.myAddr = pTxData[3];
                  basicRfConfig.myAddr = basicRfConfig.myAddr<<8; 
                  basicRfConfig.myAddr |= pTxData[2];
                  if(basicRfInit(&basicRfConfig)==FAILED) {
                    HAL_ASSERT(FALSE);
                  }
                  break;                                                                                        
                case RADIO_SET_PAN_ID:
                  basicRfConfig.panId = pTxData[3];
                  basicRfConfig.panId = basicRfConfig.panId<<8; 
                  basicRfConfig.panId |= pTxData[2];
                  if(basicRfInit(&basicRfConfig)==FAILED) {
                    HAL_ASSERT(FALSE);
                  }
                  break;                                                                                        
                case RADIO_SET_TIMEOUT:
                  wMcuResetTimeout = pTxData[3];
                  wMcuResetTimeout = wMcuResetTimeout<<8; 
                  wMcuResetTimeout |= pTxData[2];      
                  mSecCounter2 = wMcuResetTimeout;                                                                        
                  break;                                                                                
                default:
                  break;	
                }
                break;
              }
              P1_3 = 0;       //signal that is ready to receive on UART
            }
            else
            {
              wUartRcvdBytesOld = wUartRcvdBytesNew;
            }
          }
          else
          {
            wUartRcvdBytesOld = wUartRcvdBytesNew;
          }
          //				halLedClear(1);
          
          appState = IDLE;
          
        }
      }
      
		if(!basicRfPacketIsReady())
		{
			if(FSMSTAT0 ==0X11)
			{
				RFERRF = 0;		// ADDED DD
				RFIRQF0 = 0;	// ADDED DD
				RFIRQF1 = 0;	// ADDED DD
				basicRfReceiveOn();
			}
		}
		else
		{
			if((bPacketLength = basicRfReceive(pRxData, APP_PAYLOAD_LENGTH, NULL))>0) 
			{
//				basicRfReceiveOn();
//				uart0Send((unsigned char*)&pRxData[10], bPacketLength-10);
//				halUartWrite(&pRxData[10],bPacketLength-10);
				ParseRfPAcket(pRxData);
				if(!bBSLMode)
				{
//					halUartWrite(pRxData,bPacketLength);
					uart0Send((unsigned char*)pRxData, bPacketLength);
				}
				else
				{
					mSecCounter1 = BSL_MODE_TIMEOUT;
				}
			}
		}
		
		if(bBSLMode)
		{
			if(mSecCounter1 ==0)
			{
				Reset_MSP430F5419A_Application();
				bBSLMode = false;
				wPacketLen = WriteAppModeEnteredResp(pRxData);
				SendRfPacket(pRxData, wPacketLen);
				
			}
                        wMcuResetTimeout = SECOND_COMM_TIMEOUT;
			mSecCounter2 = wMcuResetTimeout;
		}
		else
		{
			if(mSecCounter2 == 0)
			{
				Reset_MSP430F5419A_Application();
                                wMcuResetTimeout = SECOND_COMM_TIMEOUT;
				mSecCounter2 = wMcuResetTimeout;
                                bResetCounter++;
                                if(bResetCounter>RESET_TIMES_BEFORE_SLEEP)
                                {
                                  PowerDownMode();
                                }
			}
		}
                
                if(bReceiverOff == true)
                {
                  if(mSecCounter3 == 0)
                  {
                    bReceiverOff = false; 
                    basicRfReceiveOn();
                  }
                }
    }
}


/***********************************************************************************
* @fn          main
*
* @brief       This is the main entry of the "Light Switch" application.
*              After the application modes are chosen the switch can
*              send toggle commands to a light device.
*
* @param       basicRfConfig - file scope variable. Basic RF configuration
*              data
*              appState - file scope variable. Holds application state
*
* @return      none
*/
void main(void)
{
	appState = IDLE;
	mSecPollUart = UART_POLLING_INTERVAL;
    
	// for next rev uncomment the following
	// first set the port value (P0_2)to '0' and then set the pin to output
	// at reset the PORT reg is 0xFF and if you set the pin to output first
	// the value will be '1' and will reset the MSP430
	            
	P0_2 = 0;		// set the pin low - MSP430_RESET - active high
//	P0DIR|=BIT2;	
	P0_3 = 0;
	P0DIR|=BIT3 |BIT2;	

	P1_3 = 0;	
	P1DIR|=BIT3;

        
    // Config basicRF
    basicRfConfig.panId = PAN_ID;
    basicRfConfig.channel = RF_CHANNEL;
    basicRfConfig.ackRequest = TRUE;
#ifdef SECURITY_CCM
    basicRfConfig.securityKey = key;
#endif

		
// initialize UART 1 for a BSL communication with MSP430
// Baud rate is fixed to 9600 baud in half-duplex mode (one sender at a time).
// Start bit, 8 data bits (LSB first), an even parity bit, 1 stop bit.
// errata - The BSL does not expect a parity bit.	
	uartProtConfig.uartNum =  1;	// uart1
	uartProtConfig.START =	0;	// low start bit
	uartProtConfig.STOP = 1;	// high stop bit
	uartProtConfig.SPB	= 0;	// 1 stop bit
	uartProtConfig.PARITY = 0;	// parity disabled
	uartProtConfig.BIT9 = 0;	// 8 bit transfer
	uartProtConfig.D9 = 0;
	uartProtConfig.FLOW = 0;	// flow control disabled
	uartProtConfig.ORDER = 0;
    // Initalise board peripherals
    halBoardInit();

    // Initalise hal_rf
    if(halRfInit()==FAILED) {
      HAL_ASSERT(FALSE);
    }

	appSelectAntenna(ANTENNA_A);
#ifdef PROGRAM_END_DEVICE
	appConfigTimer(RF_TX_PERIOD);
        
 	// No return from here
    appSwitch();
#endif	
	
   // Role is undefined. This code should not be reached
    HAL_ASSERT(FALSE);
}



void appSelectAntenna(uint8 bAntennaSel)
{
	switch(bAntennaSel)
	{
		case ANTENNA_A:	// switch to A
			bAntennaState = ANTENNA_A;
			 HAL_ANT_SW_SET_1();
			 HAL_ANT_SW__CLR_2();
			break;
		case ANTENNA_B:	// switch to B
			bAntennaState = ANTENNA_B;
			 HAL_ANT_SW_SET_2();
			 HAL_ANT_SW__CLR_1();
			break;
		default:
			break;			
	}
}

void appToggleAntennaSwitch(void)
{
	bAntennaState ^=1;
	// toggle switch
	HAL_ANT_SW__TGL_1();
	HAL_ANT_SW__TGL_2();
	
}

static void appConfigTimer(uint16 cycles)
{
    halTimer32kInit(cycles);
    halTimer32kIntConnect(&appTimerISR);
}

/***********************************************************************************
* @fn          appTimerISR
*
* @brief       32KHz timer interrupt service routine. Signals PER test transmitter
*              application to transmit a packet by setting application state.
*
* @param       none
*
* @return      none
*/
static void appTimerISR(void)
{
//    appState = POLL_UART_DATA;
	if(mSecCounter1)
		mSecCounter1--;
	if(mSecCounter2)
		mSecCounter2--;
	if(mSecCounter3)
		mSecCounter3--;        
    if(mSecPollUart==0)
    {
       appState = POLL_UART_DATA;
       mSecPollUart = UART_POLLING_INTERVAL;
    }
    else
    {
		mSecPollUart--;        
    }
}

static void appUART0Init(void)
{
	// Configure UART0 for Alternative 1 => Port P0 (PERCFG.U0CFG = 0)
	PERCFG &= ~0x01;
	// Configure relevant Port P0 pins for peripheral function:
	// P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
	P0SEL |= 0x3C;
	P0DIR |= BIT3;
	// Configure relevant Port P1 pins back to GPIO function
	P1SEL &= ~0x3C;
	
	// Initialize bitrate (U0BAUD.BAUD_M, U0GCR.BAUD_E)
	U0BAUD = uartBaudM;
	U0GCR = (U0GCR&~0x1F) | uartBaudE;
}

// This function initializes the UART protocol (start/stop bit, data bits,
// parity, etc.). The application must call this function with an initialized
// data structure according to the code in Figure 12.
void uartInitProtocol(UART_PROT_CONFIG* uartProtConfig) 
{
	// Initialize UART protocol for desired UART (0 or 1)
	if (uartProtConfig->uartNum == 0) {
		// USART mode = UART (U0CSR.MODE = 1)
		U0CSR |= 0x80;
		// Start bit level = low => Idle level = high (U0UCR.START = 0)
		// Start bit level = high => Idle level = low (U0UCR.START = 1)
		U0UCR = (U0UCR&~0x01) | uartProtConfig->START;
		// Stop bit level = high (U0UCR.STOP = 1)
		// Stop bit level = low (U0UCR.STOP = 0)
		U0UCR = (U0UCR&~0x02) | (uartProtConfig->STOP << 1);
		// Number of stop bits = 1 (U0UCR.SPB = 0)
		// Number of stop bits = 2 (U0UCR.SPB = 1)
		U0UCR = (U0UCR&~0x04) | (uartProtConfig->SPB << 2);
		// Parity = disabled (U0UCR.PARITY = 0)
		// Parity = enabled (U0UCR.PARITY = 1)
		U0UCR = (U0UCR&~0x08) | (uartProtConfig->PARITY << 3);
		// 9-bit data disable = 8 bits transfer (U0UCR.BIT9 = 0)
		// 9-bit data enable = 9 bits transfer (U0UCR.BIT9 = 1)
		U0UCR = (U0UCR&~0x10) | (uartProtConfig->BIT9 << 4);
		// Level of bit 9 = 0 (U0UCR.D9 = 0), used when U0UCR.BIT9 = 1
		// Level of bit 9 = 1 (U0UCR.D9 = 1), used when U0UCR.BIT9 = 1
		// Parity = Even (U0UCR.D9 = 0), used when U0UCR.PARITY = 1
		// Parity = Odd (U0UCR.D9 = 1), used when U0UCR.PARITY = 1
		U0UCR = (U0UCR&~0x20) | (uartProtConfig->D9 << 5);
		// Flow control = disabled (U0UCR.FLOW = 0)
		// Flow control = enabled (U0UCR.FLOW = 1)
		U0UCR = (U0UCR&~0x40) | (uartProtConfig->FLOW << 6);
		// Bit order = MSB first (U0GCR.ORDER = 1)
		// Bit order = LSB first (U0GCR.ORDER = 0) => For PC/Hyperterminal
		U0GCR = (U0GCR&~0x20) | (uartProtConfig->ORDER << 5);
	} else {
		// USART mode = UART (U1CSR.MODE = 1)
		U1CSR |= 0x80;
		// Start bit level = low => Idle level = high (U1UCR.START = 0)
		// Start bit level = high => Idle level = low (U1UCR.START = 1)
		U1UCR = (U1UCR&~0x01) | uartProtConfig->START;
		// Stop bit level = high (U1UCR.STOP = 1)
		// Stop bit level = low (U1UCR.STOP = 0)
		U1UCR = (U1UCR&~0x02) | (uartProtConfig->STOP << 1);
		// Number of stop bits = 1 (U1UCR.SPB = 0)
		// Number of stop bits = 2 (U1UCR.SPB = 1)
		U1UCR = (U1UCR&~0x04) | (uartProtConfig->SPB << 2);
		// Parity = disabled (U1UCR.PARITY = 0)
		// Parity = enabled (U1UCR.PARITY = 1)
		U1UCR = (U1UCR&~0x08) | (uartProtConfig->PARITY << 3);
		// 9-bit data enable = 8 bits transfer (U1UCR.BIT9 = 0)
		// 9-bit data enable = 8 bits transfer (U1UCR.BIT9 = 1)
		U1UCR = (U1UCR&~0x10) | (uartProtConfig->BIT9 << 4);
		// Level of bit 9 = 0 (U1UCR.D9 = 0), used when U1UCR.BIT9 = 1
		// Level of bit 9 = 1 (U1UCR.D9 = 1), used when U1UCR.BIT9 = 1
		// Parity = Even (U1UCR.D9 = 0), used when U1UCR.PARITY = 1
		// Parity = Odd (U1UCR.D9 = 1), used when U1UCR.PARITY = 1
		U1UCR = (U1UCR&~0x20) | (uartProtConfig->D9 << 5);
		// Flow control = disabled (U1UCR.FLOW = 0)
		// Flow control = enabled (U1UCR.FLOW = 1)
		U1UCR = (U1UCR&~0x40) | (uartProtConfig->FLOW << 6);
		// Bit order = MSB first (U1GCR.ORDER = 1)
		// Bit order = LSB first (U1GCR.ORDER = 0) => For PC/Hyperterminal
		U1GCR = (U1GCR&~0x20) | (uartProtConfig->ORDER << 5);
	}
}

void uart0Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength)
{
	unsigned short uartTxIndex;
	UTX0IF = 0;
	for (uartTxIndex = 0; uartTxIndex < uartTxBufLength; uartTxIndex++) {
		U0DBUF = uartTxBuf[uartTxIndex];
		while( !UTX0IF );
		UTX0IF = 0;
	}
//		U0DBUF = '\r';
//		while( !UTX0IF );
//		UTX0IF = 0;
//		U0DBUF = '\n';
//		while( !UTX0IF );
//		UTX0IF = 0;

}

// BSL Entry Sequence for MSP430F5419A
// refer to TI app note slau319c.pdf figure 1-2
// PCB-0583-10-2V3 has an inverter between P0.2 and RST
// 
void Reset_MSP430F5419A_BSL(void)
{
	// set the pins as outputs
	// P0.2 connected to RST	- P0.2 = 1 -> RST = 0
	// P0.3 connected to TEST
	//set the pins low to start the sequence
	P0_2 = 1;	//set RST low
	P0_3 = 0;
	P0DIR|=BIT3 |BIT2;	
//	P0_2 = 0;
//	P0_3 = 0;
	halMcuWaitMs(10);
	P0_3 = 1;	// set TEST high
	halMcuWaitUs(100);
	P0_3 = 0;	// set TEST low
	halMcuWaitUs(10);
	P0_3 = 1;	// set TEST high
	halMcuWaitUs(100);
//	P0_2 = 1;	// set RST high
	P0_2 = 0;	// set RST high
	halMcuWaitUs(300);
	P0_3 = 0;	// set TEST low
	halMcuWaitMs(1);
//	P0DIR&=~(BIT3 |BIT2);	// set P0.2 and P0.3 as inputs
	
}

// Application Entry Sequence for MSP430F5419A
// refer to TI app note slau319c.pdf 
// PCB-0583-10-2V3 has an inverter between P0.2 and RST
void Reset_MSP430F5419A_Application(void)
{
	// set the pins as outputs
	// P0.2 connected to RST
	// P0.3 connected to TEST
	P0DIR|=BIT3 |BIT2;	
//	P0_2 = 1;	//set RST high
	P0_2 = 0;	//set RST high
	P0_3 = 0;	//set Test low
	halMcuWaitMs(20);
//	P0_2 = 0;	// set RST low
	P0_2 = 1;	// set RST low
	halMcuWaitUs(400);
//	P0_2 = 1;	//set RST high
	P0_2 = 0;	//set RST high
	halMcuWaitMs(10);
//	P0DIR&=~(BIT3 |BIT2);	// set P0.2 and P0.3 as inputs
	
}

void GetMSP430_BSL_Version(void)
{
		
	TxBuffer[0] = 0x80;
	TxBuffer[1] = 0x01;
	TxBuffer[2] = 0x00;
	TxBuffer[3] = 0x19;
	crcReset();
	crcNextByte(0x19);
	TxBuffer[4] = crcGetLowByte();
	TxBuffer[5] = crcGetHighByte();
	
	//URX1IF = 0;
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuffer, 6);
	halMcuWaitMs(20);
//	U1CSR |= 0x40;
//	index1 = 11;
//	index2 = 0;
//	
//	while(index1--)
//	{
//		//while(!URX1IF);
//		while(!(U1CSR&0x04));
//		URX1IF = 0;
//		RxBuffer[index2++]= U1DBUF;
//			
//	}
	
	
}

static void appUART1Init(void)
{
	// Configure UART1 for Alternative 0 => Port P0 (PERCFG.U1CFG = 0)
	PERCFG &= ~0x02;
	// Configure relevant Port P0 pins for peripheral function:
	// P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
	P0SEL |= BIT4|BIT5;
	P0DIR |= BIT4;
	P0DIR &= ~BIT5;
//	// Configure relevant Port P1 pins back to GPIO function
//	P1SEL &= ~0x3C;
	
	// Initialize bitrate (U0BAUD.BAUD_M, U0GCR.BAUD_E)
	U1BAUD = uartBaudM_9600;
	U1GCR = (U1GCR&~0x1F) | uartBaudE_9600;
}

void uart1Send(unsigned char* uartTxBuf, unsigned short uartTxBufLength)
{
	unsigned short uartTxIndex;
	UTX1IF = 0;
	for (uartTxIndex = 0; uartTxIndex < uartTxBufLength; uartTxIndex++) {
		U1DBUF = uartTxBuf[uartTxIndex];
		while( !UTX1IF );
		UTX1IF = 0;
	}


}

uint16 GetMSP430_BSL_BufferSize(void)
{
		
	TxBuffer[0] = 0x80;
	TxBuffer[1] = 0x01;
	TxBuffer[2] = 0x00;
	TxBuffer[3] = 0x1A;
	crcReset();
	crcNextByte(0x1A);
	TxBuffer[4] = crcGetLowByte();
	TxBuffer[5] = crcGetHighByte();
	
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuffer, 6);
	halMcuWaitMs(20);
	
}

//RX password to unlock BSL
//Host: 80 11 00 11 FF FF FF FF FF FF FF FF FF FF FF FF FF FF 00 5C 38 4F
//BSL: 00 80 02 00 3B 00 60 C4
void SetMSP430_BSL_Password(void)
{

	char index ;
		
	TxBuffer[0] = 0x80;
	TxBuffer[1] = 0x21;
	TxBuffer[2] = 0x00;
	TxBuffer[3] = 0x11;
	TxBuffer[4] = 0xFF;
	TxBuffer[5] = 0xFF;
	TxBuffer[6] = 0xFF;
	TxBuffer[7] = 0xFF;
	TxBuffer[8] = 0xFF;
	TxBuffer[9] = 0xFF;
	TxBuffer[10] = 0xFF;
	TxBuffer[11] = 0xFF;
	TxBuffer[12] = 0xFF;
	TxBuffer[13] = 0xFF;
	TxBuffer[14] = 0xFF;
	TxBuffer[15] = 0xFF;
	TxBuffer[16] = 0xFF;
	TxBuffer[17] = 0xFF;
	TxBuffer[18] = 0xFF;
	TxBuffer[19] = 0xFF;
	TxBuffer[20] = 0xFF;
	TxBuffer[21] = 0xFF;
	TxBuffer[22] = 0xFF;
	TxBuffer[23] = 0xFF;
	TxBuffer[24] = 0xFF;
	TxBuffer[25] = 0xFF;
	TxBuffer[26] = 0xFF;
	TxBuffer[27] = 0xFF;
	TxBuffer[28] = 0xFF;
	TxBuffer[29] = 0xFF;
	TxBuffer[30] = 0xFF;
	TxBuffer[31] = 0xFF;
	TxBuffer[32] = 0xFF;
	TxBuffer[33] = 0xFF;
	TxBuffer[34] = 0xFF;
	TxBuffer[35] = 0xFF;


	crcReset();
	for(index = 3;index<=35;index++)
		crcNextByte(TxBuffer[index]);
	
	TxBuffer[36] = crcGetLowByte();
	TxBuffer[37] = crcGetHighByte();
	
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuffer, 38);
	halMcuWaitMs(100);
		
}



void MSP430_BSL_Erase(void)
{
			
	TxBuffer[0] = 0x80;
	TxBuffer[1] = 0x01;
	TxBuffer[2] = 0x00;
	TxBuffer[3] = 0x15;
	crcReset();
	crcNextByte(0x15);
	TxBuffer[4] = crcGetLowByte();
	TxBuffer[5] = crcGetHighByte();
	
	//URX1IF = 0;
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuffer, 6);
	halMcuWaitMs(100);
	
}

void MSP430_BSL_Get_Buf_Size(void)
{
			
	TxBuffer[0] = 0x80;
	TxBuffer[1] = 0x01;
	TxBuffer[2] = 0x00;
	TxBuffer[3] = 0x1A;
	crcReset();
	crcNextByte(0x1A);
	TxBuffer[4] = crcGetLowByte();
	TxBuffer[5] = crcGetHighByte();
	
	//URX1IF = 0;
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuffer, 6);
	halMcuWaitMs(100);
	
}



HAL_ISR_FUNCTION(uart1RxISR,URX1_VECTOR)
{
    uint8 c, s;

    URX1IF = 0;  
    c= U1DBUF;
    s= U1CSR;

   RxBufPut(c);
}


void RxBufPut( char data)
{
	if (RxBufIndex < RX_DATA_SIZE)
		RxBuffer[RxBufIndex++] = data;
}

void rRxBufGetAllData( char *buf)
{
	uint8 bIndex;
	for(bIndex=0;bIndex <RxBufIndex; bIndex++)
	{
		*buf = RxBuffer[bIndex];
		buf++;
	}
	RxBufReset();
}


void Uart1EnableRxInterrupt(void)
{
	URX1IF = 0;       
	U1CSR |= 0x40;
	URX1IE = 1;
}

void RxBufReset(void)
{
	RxBufIndex = 0;
}


uint8 WriteBSLModeEnteredResp(uint8 *pData)
{
	pData[0] = WTTTS_PACKET_HEADER;	// packet header
	pData[1] = BSL_MODE_ENTERED;		// packet type
	pData[2] = 0x00;	// Sequence Number 
	pData[3] = 0x00;	//WTTTS Timestamp Byte 1
	pData[4] = 0x00;	//WTTTS Timestamp Byte 2
	pData[5] = 0x07;	//Packet Size
	pData[6] = 0x60;	// data
	pData[7] = 0x80;	// data
	pData[8] = 0x01;	// data
	pData[9] = 0x00;	//data
	pData[10] = 0x40;	//data
	pData[11] = 0x34;	//data
	pData[12] = 0xA9;	//data
	pData[13] = 0x00;	//Checksum
	
	return BSL_MODE_ENTERED_LENGTH;
}

uint8 WriteAppModeEnteredResp(uint8 *pData)
{
	pData[0] = WTTTS_PACKET_HEADER;	// packet header
	pData[1] = APP_MODE_ENTERED;		// packet type
	pData[2] = 0x00;	// Sequence Number 
	pData[3] = 0x00;	//WTTTS Timestamp Byte 1
	pData[4] = 0x00;	//WTTTS Timestamp Byte 2
	pData[5] = 0x06;	//Packet Size
	pData[6] = 0x80;	// data
	pData[7] = 0x01;	// data
	pData[8] = 0x00;	// data
	pData[9] = 0x40;	//data
	pData[10] = 0x34;	//data
	pData[11] = 0xa9;	//data
	pData[12] = 0x00;	//checksum
	
	
	return APP_MODE_ENTERED_LENGTH;
}

uint8 WriteProgramDataResp(uint8 *pData)
{
	uint8 bCnt = 0;
	
	pData[0] = WTTTS_PACKET_HEADER;	// packet header
	pData[1] = BSL_PROGRAM_DATA;		// packet type
	pData[2] = 0x00;	// Sequence Number 
	pData[3] = 0x00;	//WTTTS Timestamp Byte 1
	pData[4] = 0x00;	//WTTTS Timestamp Byte 2
	pData[5] = RxBufIndex;	//Packet Size
	rRxBufGetAllData(&pData[6]);
	bCnt = 6+pData[5];
	pData[bCnt] = 0x00;	//Checksum
	
	bCnt++;
	return bCnt;
	
}


void ParseRfPAcket(uint8 *pData)
{
	
	if(pData[0]!=HOST_PACKET_HEADER)
		return;
        bBslTxRetry = 0;
	switch(pData[1])
	{
		case ENTER_BSL_MODE:
				Reset_MSP430F5419A_BSL();
				bBSLMode = true;
				wPacketLen = WriteBSLModeEnteredResp(pTxData);
				SendRfPacket(pTxData, wPacketLen);
			break;
		case BSL_PROGRAM_DATA:
				SendDataToMcu(&pData[6], pData[5]);
				wPacketLen = WriteProgramDataResp(pTxData);
				SendRfPacket(pTxData, wPacketLen);
			break;			
		case ENTER_APP_MODE:
				Reset_MSP430F5419A_Application();
				bBSLMode = false;
				wPacketLen = WriteAppModeEnteredResp(pTxData);
				SendRfPacket(pTxData, wPacketLen);
			break;				
		default:
			
			break;	
	}
}

void SendDataToMcu(unsigned char* TxBuf, unsigned short TxBufLength)
{
	RxBufReset();
	Uart1EnableRxInterrupt();
	uart1Send(TxBuf, TxBufLength);
	halMcuWaitMs(100);
}

bool SendRfPacket(uint8 *pData, uint16 wPackLen)
{
        bBslTxRetry = 1;
	if(basicRfSendPacket(LIGHT_ADDR, pData, wPackLen)==FAILED)
	{
		appToggleAntennaSwitch();
                bBslTxRetry++;
		if(basicRfSendPacket(LIGHT_ADDR, pData, wPackLen)==FAILED)
		{
			appToggleAntennaSwitch();
                        bBslTxRetry++;
			if(basicRfSendPacket(LIGHT_ADDR, pData, wPackLen)==FAILED)
			{	
				appToggleAntennaSwitch();
                                bBslTxRetry++;
				return false;
			}
		}
	}
	return true;
}


void PowerDownMode(void)
{
	// Put MCU to sleep. It will wake up on joystick interrupt
	halIntOff();
	SLEEPCMD |= POWER_MODE3;
	PCON = 0x01;

	
	
}




void TestBSLMode(void)
{
// TEST BSL MODE
uint16 wCounter;
	while(1)
	{
		appUART1Init();
		P0INP&=~(BIT4|BIT5);
		uartInitProtocol(&uartProtConfig) ;
		Reset_MSP430F5419A_BSL();
		wCounter = 20000;
		while(wCounter--);
		MSP430_BSL_Erase();
		wCounter = 10000;
		while(wCounter--);
		SetMSP430_BSL_Password();
		wCounter = 10000;
		while(wCounter--);
		GetMSP430_BSL_Version();
		wCounter = 10000;
		while(wCounter--);
		MSP430_BSL_Get_Buf_Size();
		
	}
// END of Test BSL

}



/****************************************************************************************
  Copyright 2007 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
***********************************************************************************/
